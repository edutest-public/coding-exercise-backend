# Coding Exercise - Backend Developer

## Overview

Building upon this base project, implement a Spring Boot based REST API Server to render a **CRUD Operation for Multi-Level Hierarchy based Navigation Menu**.

## What needs to be done

 - Use MySQL relational database to store the information as mentioned below:
    - Menu
        - Unique Id
        - Menu Name (text)
        - URL Link (text)
        - Way to indicate if it is a top level menu or a child menu
        - If child menu, then who is my parent, i.e. Parent Menu Id
 - We should be able to execute CRUD operations using appropriate REST standards to:
    - Create, Read, Update or Delete any Menu Item
    - The "list all menu items" API should return a JSON that contains menus in the hierarchical order **and not** as a simple object dump, e.g.:
        ```
        [
            {
                "id": 1,
                "name": "Home",
                "linkUrl": "/"
            },
            {
                "id": 2,
                "name": "Technologies",
                "linkUrl": "#",
                "subMenus: [
                    {
                        "id": 3,
                        "name": "Go Lang",
                        "linkUrl": "/go",
                    },
                    {
                        "id": 4,
                        "name": "Java",
                        "linkUrl": "/java",
                    },
                    {
                        "id": 5,
                        "name": "JavaScript",
                        "linkUrl": "/#",
                        "subMenus: [
                            {
                                "id": 6,
                                "name": "React",
                                "linkUrl": "/react",
                            },
                            {
                                "id": 7,
                                "name": "Angular",
                                "linkUrl": "/angular",
                            },
                            {
                                "id": 8,
                                "name": "Node",
                                "linkUrl": "/node",
                            }
                        ]
                    }
                ]
            },
            {
                "id": 9,
                "name": "About",
                "linkUrl": "/about"
            }
        ]
        ```

## Evaluation Criteria

The solution should implement as many of the criteria points outlined below:

- Overall solution design.
- Logic and Performance of the Hierarchy Listing API of Menus
- Test cases written.
- Containerization, using Docker. You may use `docker-compose` or similar tools to achieve the same.

*You may choose to use Gradle instead of Maven. This will not have any impact on overall evaluation. You may also choose Node JS or Go Lang as the main technolgy stack instead of Java.*
- *[Go Micro Sample Project](https://gitlab.com/gitlab-org/project-templates/go-micro)*
- *[Node JS Sample Project](https://gitlab.com/gitlab-org/project-templates/express)*
- *[Spring Boot Sample Project](https://gitlab.com/gitlab-org/project-templates/spring)*

## Submitting the Solution

Your solution should be submitted by forking this project, and submitting a new Merge Request **before** the communicated deadline.